[![Build Status](https://travis-ci.org/serihiro/js_learning.png?branch=master)](https://travis-ci.org/serihiro/js_learning)
[![Coverage Status](https://coveralls.io/repos/serihiro/js_learning/badge.png?branch=master)](https://coveralls.io/r/serihiro/js_learning?branch=master)
# Javascript learning with jasmine

This repository contains followings :
* Javascript test cases used with jasmine
* My notes about javascript


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/serihiro/js_learning/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

