itanbul=./node_modules/.bin/istanbul
jasmine=./node_modules/jasmine-node/bin/jasmine-node

test-cov: clean
	  $(itanbul) cover $(jasmine) --verbose spec

coveralls: 
	  cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js;

clean:
	  rm -fr coverage
