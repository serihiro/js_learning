describe('function define test',function(){
  
  it('function returns 2',function(){
    function hoge(){
      return 2;
    }
    expect(hoge()).toBeDefined();
    expect(2).toEqual(hoge());
  });

  
  it('可変長引数',function(){
    function showMessage(){
      for(var i in arguments){
	console.log(arguments[i]);
      }
    }
  
    showMessage('hoge','fuga');//'hoge fuga'
    showMessage('hoge');//'hoge'
    showMessage();//''
    showMessage(null);//'null'
    showMessage(undefined);//'null'

  });

  it('引数をオブジェクトとして取る',function(){
    function triangle(args){
      if(args.width == null || args.width == undefined){args.width =2;}
      if(args.height == null || args.height == undefined){args.height =5;}

      return args.width * args.height / 2;
    }
  
    expect(triangle({width:5,height:4})).toEqual(10);
    expect(triangle({height:4,width:5})).toEqual(10);
    expect(triangle({height:4})).toEqual(4);
    expect(triangle({width:4})).toEqual(10);
    expect(triangle({})).toEqual(5);
  });
  
  
  it('closure',function(){
    function closure(num){
      var counter = num;

      return function(){
	return ++counter;
      }
    }

    var closure1 = closure(1);
    expect(closure1()).toEqual(2);
    expect(closure1()).toEqual(3);
    
    var closure100 = closure(100);
    expect(closure100()).toEqual(101);
    expect(closure100()).toEqual(102);
  });



});
