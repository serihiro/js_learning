describe('Javascirpt Basic', function() {
 
  describe('String', function() {
   
    it('should say "Hello World"' , function(){
      expect('Hello World').toEqual('Hello World');
    });  

    it('should "Hello World" length return 11 ' ,function(){
      expect(10).not.toEqual('Hello World'.length);
      expect(11).toEqual('Hello World'.length);
      expect(12).not.toEqual('Hello World'.length);
    });
  
  });

  describe('Array' , function(){
    beforeEach(function(){
      arrayA = ['hoge' , 'foo' , 'huga'];
      arrayB = null;
    });

    it('should return "hoge" from arrayA first attribute' , function(){
      expect('hoge').toEqual(arrayA[0]);
    });

    it('should return "foo" from arrayA second attribute' , function(){
      expect('foo').toEqual(arrayA[1]);
    });

    it('should return "huga" from arrayA second attribute' , function(){
      expect('huga').toEqual(arrayA[2]);
    });

    it('should get 3 from arrayA.length' , function(){
      expect(3).toEqual(arrayA.length);
    });

    it('should get hoge from arrayB when arrayA is copied to arrayB' , function(){
      arrayB = arrayA;
      expect('hoge').toEqual(arrayB[0]);
    });
    
    it('should be "11" when "1" + 1' , function(){
      var a = 1;
      var b = '1';
      expect('11').toEqual(a + b);
    });
  });

});
