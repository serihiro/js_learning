describe('boolean', function() {
  
  describe('String',function(){
    it('should be true with string "a" ',function(){
      expect('a').toBeTruthy(); 
    });
  
    it('should be false with string "" ',function(){
      expect('').toBeFalsy(); 
    });
 
    it('should be true with string "\\n"',function(){
      expect("\n").toBeTruthy(); 
    });
 
    it('should be true with string "\\r" ',function(){
      expect("\r").toBeTruthy(); 
    });
    
    it('should be true with string "\\t" ',function(){
      expect("\t").toBeTruthy(); 
    });
  });

  describe('Integer',function(){
    it('should be true with integer 1',function(){
      expect(1).toBeTruthy();
    });

    it('should be false with integer 0',function(){
      expect(0).toBeFalsy();
    });
    
    it('should be true with integer -1',function(){
      expect(-1).toBeTruthy();
    });
  });

  describe('Array',function(){
    it('should be true with ["a"]',function(){
      expect(['a']).toBeTruthy();
    });
    
    it('should be true with [""]',function(){
      expect(['']).toBeTruthy();
    });
    
    it('should be true with []',function(){
      expect([]).toBeTruthy();
    });

    it('should be ture with new Array()',function(){
      expect(new Array()).toBeTruthy();
    });
  });

  describe('Object',function(){
    it('should be true with new Object()',function(){
      expect(new Array()).toBeTruthy();
    });
    
    it('should be true with {}',function(){
      expect({}).toBeTruthy();
    });

    it('should be true with {a:"hoge"}',function(){
      expect({a:'hoge'}).toBeTruthy();
    });
  });

  describe('Other',function(){
    it('should be false with undefined',function(){
      expect(undefined).toBeFalsy();
    });

    it('should be false with null',function(){
      expect(null).toBeFalsy();
    });
  });
});
